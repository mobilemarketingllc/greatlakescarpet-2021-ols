<?php
/*
Template Name: Grab N Go Page 
*/

get_header(); 
?>

<div class="fl-content-full container">
   <div class="row">
       <div class="fl-content col-md-12">
           <?php
           echo do_shortcode('[fl_builder_insert_layout slug="grab-and-go-template"]');
           ?>
       </div>
   </div>
</div>

<?php get_footer(); ?>