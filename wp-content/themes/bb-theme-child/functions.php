<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

//Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("calendly_popup","https://assets.calendly.com/assets/external/widget.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    //wp_enqueue_script("child-s", "https://greatlakescarpet-stg.mm-dev.agency/wp-content/plugins/grand-child/js/lightgallery-all.min.js","","",0);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Hook to the filter
add_filter('wpseo_breadcrumb_links', 'wpse_332125_breadcrumbs');
// $links are the current breadcrumbs
function wpse_332125_breadcrumbs($links) {
    if(is_singular('area_rugs')) {
        global $post;
        $product_name = $post->post_title;
        // The first item in $links ($links[0]) is Home, so skip it
        // The second item in $links is Projects - we want to change that
        $links[1] = array('text' => 'Flooring', 'url' => '/flooring/', 'allow_html' => 1);
        $links[2] = array('text' => 'Area Rugs', 'url' => '/flooring/area-rugs/', 'allow_html' => 1);
        $links[3] = array('text' => 'Products', 'url' => '/flooring/area-rugs/products/', 'allow_html' => 1);
        $links[4] = array('text' => $product_name, 'allow_html' => 1);
    }
    // Even if we didn't change anything, always return the breadcrumbs
    return $links;
}



//301 redirect added from 404 logs table
wp_clear_scheduled_hook( '404_redirection_log_cronjob' );
wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('greatlake_404_redirection_301_log_cronjob')) {
    
    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time() +  17800, 'daily', 'greatlake_404_redirection_301_log_cronjob');
}

add_action( 'greatlake_404_redirection_301_log_cronjob', 'greatlake_custom_404_redirect_hook' ); 

function custom_check_404($url) {
   $headers=get_headers($url, 1);
  if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function greatlake_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring/carpet/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring/hardwood/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring/laminate/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url = '/flooring/vinyl/products/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url = '/flooring/vinyl/products/';
            }
            
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url = '/flooring/tile/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

        else if (strpos($row_404->url,'area_rugs') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = custom_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url = '/flooring/area-rugs/products/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $source_url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'rugs 301 added ');
        }

       }  
    }

 }

 
  //IP location FUnctionality
//   if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
//     wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
// }

add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location(){

          global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

              write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['name']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}



//get ipaddress of visitor

function getUserIpAddr() {
  $ipaddress = '';
  if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
  else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
  else
      $ipaddress = 'UNKNOWN';
  
  if ( strstr($ipaddress, ',') ) {
          $tmp = explode(',', $ipaddress,2);
          $ipaddress = trim($tmp[1]);
  }
  return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
  
  global $wpdb;
  $content="";


//  echo 'no cookie';
  
  $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
  //12.68.65.195
  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];
      if(isset($_COOKIE['preferred_store'])){

          $store_location = $_COOKIE['preferred_store'];
      }else{

          $store_location = '';
         
        }

      //print_r($rawdata);
      
      $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

              write_log($sql);

      $storeposts = $wpdb->get_results($sql);
      $storeposts_array = json_decode(json_encode($storeposts), true);      

     
      if($store_location ==''){
          //write_log('empty loc');           
          $store_location = $storeposts_array['0']['ID'];
          $store_distance = round($storeposts_array['0']['distance'],1);
      }else{

         // write_log('noempty loc');
          $key = array_search($store_location, array_column($storeposts_array, 'ID'));
          $store_distance = round($storeposts_array[$key]['distance'],1);           

      }
      
      $content .= '<div class="header_store"><div class="store_wrapper">';
      $content .='<div class="locationWrapFlyer">
      
      <div class="contentFlyer"><div> 
          <p>You\'re Store</p>';
      $content .= '<h3 class="title-prefix">'.get_the_title($store_location).'</h3>';        
      $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
      $content .= '</div></div></div>';
      
     

  $content_list = '<div id="storeLocation" class="changeLocation">';
  $content_list .= '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>';
  $content_list .= '<div class="content">'; 

  foreach ( $storeposts as $post ) {

    write_log(get_field('phone', $post->ID));
   
      $content_list .= '<div class="store_wrapper" id ="'.$post->ID.'">';
      $content_list .= '<h5 class="title-prefix">'.get_the_title($post->ID).'  - <b>'.round($post->distance,1).' MI</b></h5>';
      $content_list .= '<h5 class="store-add">'.get_field('address', $post->ID).' '.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</h5>';
      
      if(get_field('closed', $post->ID)){

          $content_list .= '<p class="store-hour redtext">'.get_field('hours', $post->ID).'</p>';
          $content_list .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
          
      }else{
         
                  $day = get_field(strtolower(date("l")), $post->ID);                     

                  if($day == 'Closed'){

                    $content_list .= '<p class="store-hour">CLOSED TODAY</p>';

                  }else{

                    write_log($day);
                    $daystr = strval($day);
                    $closedtime = explode("–",$daystr);                        
                    $content_list .= '<p class="store-hour">OPEN UNTIL '.trim($closedtime[1]).'</p>';
                  }
                
          
      }
      $content_list .= '<p class="store-hour"><span class="phoneIcon"></span> &nbsp;'.get_field('phone', $post->ID).'</p>';
      $content_list .= '<a href="javascript:void(0)" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-distance="'.round($post->distance,1).'" target="_self" class="store-cta-link choose_location">Choose This Location</a>';
      if(get_field('store_link',$post->ID)){
          $content_list .= '<br><a href="'.get_field('store_link',$post->ID).'" target="_self" class="store-cta-link view_location"> View Location</a>';
      }
      $content_list .= '</div>'; 
  }
  
  $content_list .= '</div>';
  $content_list .= '</div>';

  $data = array();

  $data['header']= $content;
  $data['list']= $content_list;

  write_log($data['list']);

  echo json_encode($data);
      wp_die();
}

//add_shortcode('storelisting', 'get_storelisting');
add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting' );



//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {
 
      $content = '<div class="store_wrapper">';
      $content .='<div class="locationWrapFlyer">
     
      <div class="contentFlyer"><div>
          <p>You\'re Store</p>';

      $content .= '<h3 class="title-prefix">'. get_the_title($_POST['store_id']) .'</h3>';
      
     $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
      $content .= '</div></div>';

      echo $content;

  wp_die();
}

//   function my_after_header() {
//       echo '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
//       <div class="locationOverlay"></div>';
//     }
//     add_action( 'fl_after_header', 'my_after_header' );

function location_header() {
    return '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
    <div class="locationOverlay"></div>';
  }
add_shortcode('location_code', 'location_header');

add_filter( 'gform_pre_render_16', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_16', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_16', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_16', 'populate_product_location_form' );

add_filter( 'gform_pre_render_19', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_19', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_19', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_19', 'populate_product_location_form' );

add_filter( 'gform_pre_render_18', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_18', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_18', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_18', 'populate_product_location_form' );

add_filter( 'gform_pre_render_33', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_33', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_33', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_33', 'populate_product_location_form' );

add_filter( 'gform_pre_render_34', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_34', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_34', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_34', 'populate_product_location_form' );

add_filter( 'gform_pre_render_35', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_35', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_35', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_35', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

foreach ( $form['fields'] as &$field ) {

    // Only populate field ID 12
    if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
        continue;
    }      	

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );										
      
         $locations =  get_posts( $args );

         $choices = array(); 

         foreach($locations as $location) {
            

              $title = get_the_title($location->ID);
              
                   $choices[] = array( 'text' => $title, 'value' => $title );

          }
          wp_reset_postdata();

         // write_log($choices);

         // Set placeholder text for dropdown
         $field->placeholder = '-- Choose Location --';

         // Set choices from array of ACF values
         $field->choices = $choices;
    
}
return $form;

}