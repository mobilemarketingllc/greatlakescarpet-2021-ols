<?php
namespace JordanLeven\Plugins\BeaversaurusRex; define('EDD_SAMPLE_PLUGIN_LICENSE_PAGE', 'beaverbuildermegamenulicense'); function edd_sample_license_menu() { add_plugins_page('BB Mega Menu License', 'BB Mega Menu License', 'manage_options', EDD_SAMPLE_PLUGIN_LICENSE_PAGE, __NAMESPACE__ . '\\edd_sample_license_page'); } function edd_sample_license_page() { $sp11e52f = get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY); $spa1f31c = get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS); ?>
    <div class="wrap">
        <h2><?php  _e('Plugin License Options'); ?>
</h2>
        <?php  if (isset($_GET['sl_activation']) && $_GET['sl_activation'] === 'false' && isset($_REQUEST['message'])) { echo sprintf('<div class="notice notice-error"><p>Error activating: %s</p></div>', $_REQUEST['message']); } ?>
        <form method="post" action="options.php">

            <?php  settings_fields('edd_sample_license'); ?>

            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row" valign="top">
                            <?php  _e('License Key'); ?>
                        </th>
                        <td>
                            <input id="beaversaurus_rex_license_key" name="beaversaurus_rex_license_key" type="text" class="regular-text" value="<?php  esc_attr_e($sp11e52f); ?>
" />
                            <label class="description" for="beaversaurus_rex_license_key"><?php  _e('Enter your license key'); ?>
</label>
                        </td>
                    </tr>
                    <?php  if (false !== $sp11e52f) { ?>
                        <tr valign="top">
                            <th scope="row" valign="top">
                                <?php  _e('Activate License'); ?>
                            </th>
                            <td>
                                <?php  if ($spa1f31c !== false && $spa1f31c == 'valid') { ?>
                                    <span style="color:green;"><?php  _e('active'); ?>
</span>
                                    <?php  wp_nonce_field('beaversaurus_rex_noonce', 'beaversaurus_rex_noonce'); ?>
                                    <input type="submit" class="button-secondary" name="beaversaurus_rex_edd_license_deactivate" value="<?php  _e('Deactivate License'); ?>
"/>
                                <?php  } else { wp_nonce_field('beaversaurus_rex_noonce', 'beaversaurus_rex_noonce'); ?>
                                    <input type="submit" class="button-secondary" name="beaversaurus_rex_edd_license_activate" value="<?php  _e('Activate License'); ?>
"/>
                                <?php  } ?>
                            </td>
                        </tr>
                    <?php  } ?>
                </tbody>
            </table>
            <?php  submit_button(); ?>

        </form>
        <?php  } function edd_sample_register_option() { register_setting('edd_sample_license', 'beaversaurus_rex_license_key', 'edd_sanitize_license'); } function edd_sanitize_license($spacddd3) { $sp6593dc = get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY); if ($sp6593dc && $sp6593dc != $spacddd3) { delete_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS); } return $spacddd3; } function edd_sample_activate_license() { if (isset($_POST['beaversaurus_rex_edd_license_deactivate'])) { delete_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY); delete_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS); } else { if (isset($_POST['beaversaurus_rex_edd_license_activate'])) { if (!check_admin_referer('beaversaurus_rex_noonce', 'beaversaurus_rex_noonce')) { return; } $sp11e52f = trim(get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY)); $sp01e61b = array('edd_action' => 'activate_license', 'license' => $sp11e52f, 'item_id' => EDD_SL_ITEM_ID, 'url' => home_url()); $sp8022c5 = wp_remote_post(EDD_SL_STORE_URL, array('timeout' => 15, 'sslverify' => false, 'body' => $sp01e61b)); if (is_wp_error($sp8022c5) || 200 !== wp_remote_retrieve_response_code($sp8022c5)) { $spb8054c = is_wp_error($sp8022c5) && !empty($sp8022c5->get_error_message()) ? $sp8022c5->get_error_message() : __('An error occurred, please try again.'); } else { $sp9bbc56 = json_decode(wp_remote_retrieve_body($sp8022c5)); if (false === $sp9bbc56->success) { switch ($sp9bbc56->error) { case 'expired': $spb8054c = sprintf(__('Your license key expired on %s.'), date_i18n(get_option('date_format'), strtotime($sp9bbc56->expires, current_time('timestamp')))); break; case 'revoked': $spb8054c = __('Your license key has been disabled.'); break; case 'missing': $spb8054c = __('Invalid license.'); break; case 'invalid': case 'site_inactive': $spb8054c = __('Your license is not active for this URL.'); break; case 'item_name_mismatch': $spb8054c = sprintf(__('This appears to be an invalid license key for %s.'), EDD_SL_ITEM_ID); break; case 'no_activations_left': $spb8054c = __('Your license key has reached its activation limit.'); break; default: $spb8054c = __('An error occurred, please try again.'); break; } } } if (!empty($spb8054c)) { $spd197e6 = admin_url('plugins.php?page=' . EDD_SAMPLE_PLUGIN_LICENSE_PAGE); $sp46e12c = add_query_arg(array('sl_activation' => 'false', 'message' => urlencode($spb8054c)), $spd197e6); wp_redirect($sp46e12c); die; } update_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS, $sp9bbc56->license); wp_redirect(admin_url('plugins.php?page=' . EDD_SAMPLE_PLUGIN_LICENSE_PAGE)); die; } } } add_action('admin_init', __NAMESPACE__ . '\\edd_sample_activate_license'); add_action('admin_init', __NAMESPACE__ . '\\edd_sample_register_option'); add_action('admin_menu', __NAMESPACE__ . '\\edd_sample_license_menu');