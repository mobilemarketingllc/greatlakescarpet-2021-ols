<div class="product-variations">
    <h3>Color Variations</h3>
   <div class="color_variations_slider">
        <div class="slides">
            <?php
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
                $post_type = get_post_type(get_the_ID());
            ?>
            <div class="slide col-md-2 col-sm-3 col-xs-6 color-box">
                <figure class="color-boxs-inner">
					<div class="color-boxs-inners">
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
                        </a>
                        <br />
                        <small><?php the_field('color'); ?></small>
                        <?php if($post_type == "Area_rugs"){ ?>
                            <br />
                        <small><?php the_field('size'); ?></small>
                        <?php } ?>
                    </div>
                </figure>
            </div>
            <?php
    } ?>
        </div>
    </div>
</div>
<?php wp_reset_postdata(); ?> 