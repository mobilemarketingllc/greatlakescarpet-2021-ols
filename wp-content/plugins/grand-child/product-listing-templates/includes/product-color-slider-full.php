<div class="productDName"><?php the_title();?></div>
<div class="product-variations">
    <div class="row">
        <?php
       
		   $value = get_post_meta($post->ID, 'collection', true);
		   $key =  "collection";
	   
	   global $post;
	   $flooringtype = $post->post_type;  
		 $args = array(
            'post_type'      => $flooringtype,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'     => $key,
                    'value'   => $value,
                    'compare' => '='
                ),
                array(
                    'key' => 'swatch_image_link',
                    'value' => '',
                    'compare' => '!='
                    )
            )
        );
        ?>
        <?php
        $the_query = new WP_Query( $args );
		
        ?>




		<div class="fr-slider color_variations_slider" data-fr='{"slidesToScroll":7,"slidesToShow":11,"arrows":false,"infinite": false}'>
            <a href="#" class="arrow prev">
		<!--		<i class="fa fa-angle-left"></i> -->
			<img src="/wp-content/plugins/grand-child/product-listing-templates/images/arrow-left.png">
			</a>
            <a href="#" class="arrow next">
			<!--	<i class="fa fa-angle-right"></i> -->
					<img src="/wp-content/plugins/grand-child/product-listing-templates/images/arrow-right.png"></a>
            <div class="slides chfl_clr_slider">
                <?php  while ( $the_query->have_posts() ) {
                $the_query->the_post(); 
                $post_type = get_post_type(get_the_ID());
                ?>
                    <div class="slide col-md-2 col-sm-3 col-xs-6 color-box">
                        <a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                         		<?php
								  $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
				                  $style= "padding: 5px;";
								?>
							
                                <img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
                         
                        </a>
                        <br />
                        <small><?php the_field('color'); ?></small>
                        <?php if($post_type == "Area_rugs"){ ?>
                            <br />
                        <small><?php the_field('size'); ?></small>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php wp_reset_postdata(); ?>


</div>
    </div>